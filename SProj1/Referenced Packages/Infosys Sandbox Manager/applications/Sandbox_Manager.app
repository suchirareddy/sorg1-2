<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <label>Sandbox Manager</label>
    <tab>Sandbox__c</tab>
    <tab>SW_Config__c</tab>
    <tab>Sandbox_Manager</tab>
    <tab>Remote_Instance_Auth</tab>
    <tab>SM_Auth__c</tab>
    <tab>relationPic__c</tab>
    <tab>light</tab>
    <tab>light12</tab>
</CustomApplication>
